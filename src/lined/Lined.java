package lined;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Lined {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("LINED - LINE EDITOR");
        
        while (true) {
            int option = getMenuOption(input);
            
            if (option == 1){
                String filePath = getFilePath(input);
                String fileContent = readFile(filePath);
                
                System.out.println("\n--------------------------------------------------------------------------------");
                
                System.out.print(fileContent);
                
                System.out.println("--------------------------------------------------------------------------------\n");
            }
            else if (option == 2) {
                String filePath = getFilePath(input);
                String fileContent = "";
                
                System.out.println("\nWrite /s in a new line and press Enter to save.");
                System.out.println("\n--------------------------------------------------------------------------------");
                
                String textLine = input.nextLine();
                while (!textLine.equals("/s")) {
                    fileContent += textLine + "\n";
                    textLine = input.nextLine();
                }
                System.out.println("--------------------------------------------------------------------------------");
                String fileAbsolutePath = writeFile(filePath, fileContent);
                System.out.println("\nThe file has been saved in this path: " + fileAbsolutePath + "\n");
            }
            else if (option == 3) {
                String filePath = getFilePath(input);
                
                //Read file
                String fileContent = readFile(filePath);
                String[] textLines = fileContent.split("\n");
                
                //Make changes                
                while (true) {
                    System.out.println("\n--------------------------------------------------------------------------------");
                    for (int i = 0; i < textLines.length; i++) {
                        System.out.println((i + 1) + ". " + textLines[i]);
                    }
                    System.out.println("--------------------------------------------------------------------------------\n");
                    
                    int editLine = getEditLine(input);
                    if (editLine <= -1) { break; }
                    
                    System.out.print("Old line: ");
                    System.out.println(textLines[editLine]);
                    
                    System.out.print("New line: ");
                    String newLine = input.nextLine();
                    textLines[editLine] = newLine;
                }
                
                //Write changes to file
                fileContent = "";
                for (int i = 0; i < textLines.length; i++) {
                    fileContent += textLines[i] + "\n";
                }
                writeFile(filePath, fileContent);
            }
            else if (option == 4) {
                input.close();
                System.exit(0);
            }
        }
    }
    
    private static int getMenuOption(Scanner input) {
        System.out.print("\nOPTIONS\n"
                       + "-------\n"
                       + "1. Read file\n"
                       + "2. Write file\n"
                       + "3. Edit file\n"
                       + "4. Close\n"
                       + "\nOption: ");
        
        int option = 0;

        try {
            option = input.nextInt();
            if (option < 1 || option > 4) {
                throw new IllegalArgumentException();
            }
        } catch (InputMismatchException | IllegalArgumentException ex) {
            System.err.println("\nError: Choose an option from 1 to 4.");
        } finally {
            input.nextLine();
        }
        
        return option;
    }
    
    private static String getFilePath(Scanner input) {
        System.out.print("\nFile path: ");
        String filePath = input.nextLine();
        
        return filePath;
    }
    
    private static int getEditLine(Scanner input) {
        System.out.print("\nLine number to edit (0 to save): ");
        int editLine = input.nextInt();
        input.nextLine();
        
        return editLine - 1;
    }

    private static String readFile (String filePath) {
        File file = new File(filePath);
        String fileContent = "";
        Scanner reader = null;
       
        try {
            reader = new Scanner(file);
            while (reader.hasNextLine()) {
                fileContent += reader.nextLine() + "\n";
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            System.err.println("\nError: " + ex.getMessage());
        }

        return fileContent;
    }
    
    private static String writeFile (String filePath, String fileContent) {
        File file = new File(filePath);
        FileWriter writer = null;
        
        try {
            writer = new FileWriter(file);
            writer.write(fileContent);
            writer.close();
            
        } catch (IOException ex) {
            System.err.println("\nError: " + ex.getMessage());
        }
        
        return file.getAbsolutePath();
    }
}